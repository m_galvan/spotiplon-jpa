package co.simplon.promo16.spotiplon.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import co.simplon.promo16.spotiplon.entity.Artist;
import co.simplon.promo16.spotiplon.repository.ArtistRepository;

@Controller
public class ArtistController {
    @Autowired
    private ArtistRepository repo;
    
    @GetMapping("/")
    public String test() {
        List<Artist> artist = repo.findAll();
        System.out.println(artist.get(0).getName());
        // Artist artist = repo.findById(1);
        // repo.delete(artist);
        // repo.delete(new Artist(2, "name","style", "picture", LocalDate.now()));
        return "test";
    }
}
