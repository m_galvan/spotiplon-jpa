package co.simplon.promo16.spotiplon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpotiplonApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpotiplonApplication.class, args);
	}

}
