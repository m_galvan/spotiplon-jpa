package co.simplon.promo16.spotiplon.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.simplon.promo16.spotiplon.entity.Artist;
/**
 * Utilisez plutôt ça
 */
@Repository
public interface ArtistRepository extends JpaRepository<Artist, Integer> {
    List<Artist> findByNameContains(String name);
    List<Artist> findByName(String name);
}
